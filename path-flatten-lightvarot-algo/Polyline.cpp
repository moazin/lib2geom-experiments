#include <Polyline.h>
#include <2geom/svg-path-writer.h>


bool Polyline::approximatesEnough(Geom::Curve &curve, Geom::Line& line, double threshold) {
    Geom::Rect bound = curve.boundsFast();
    Geom::Point p1 = bound.corner(0);
    Geom::Point p2 = bound.corner(1);
    Geom::Point p3 = bound.corner(2);
    Geom::Point p4 = bound.corner(3);
    std::vector<int> distances;
    distances.push_back(distance(p1, line));
    distances.push_back(distance(p2, line));
    distances.push_back(distance(p3, line));
    distances.push_back(distance(p4, line));
    double maximum_distance = *std::max_element(distances.begin(), distances.end());
    if(maximum_distance <= threshold) {
      return true;
    }
    return false;
}

void Polyline::splitCurve(const Geom::Curve& curve, double t_start, double t_end, double threshold) {
    Geom:: Curve *split_curve = curve.portion(t_start, t_end);
    Geom::Point start_point = curve.pointAt(t_start);
    Geom::Point end_point = curve.pointAt(t_end);
    Geom::Line line(start_point, end_point);
    if (approximatesEnough(*split_curve, line, threshold)) {
      this->addPoint(curve.pointAt(t_end));
    } else {
      double t_mid = (t_start + t_end)/2.0;
      splitCurve(curve, t_start, t_mid, threshold);
      splitCurve(curve, t_mid, t_end, threshold);
    }
}

Polyline::Polyline(Geom::Path &path, double threshold) {
  _pts.push_back(FitPoint(path.initialPoint()));
  for(auto const &curve: path) {
    splitCurve(curve, curve.timeRange()[0], curve.timeRange()[1], threshold);
  }
}

void Polyline::addPoint(Geom::Point point) {
  _pts.push_back(FitPoint(point));
}


void Polyline::initPositions(int stP, int enP) {
	_pts[stP]._l = 0;
	_pts[stP]._t = 0;
  Geom::Point lastP = _pts[stP]._pt;
	double lastT = _pts[stP]._t;
	for (int i=stP + 1; i<=enP; i++) {
    Geom::Point dir = _pts[i]._pt - lastP;
		double nl = sqrt(Geom::dot(dir, dir));
		_pts[i]._l = nl;
		_pts[i]._t = lastT + nl;
		lastP = _pts[i]._pt;
		lastT += nl;
	}
	double totL = lastT;
	for (int i=stP; i<enP; i++)
		_pts[i]._t /= totL;
}

void Polyline::calcControlPoints(int stP, int enP) {
	_P0 = _pts[stP]._pt;
	_P3 = _pts[enP]._pt;
	_P1 = _P0;
	_P2 = _P3;
	for (int i=stP+1; i<enP; i++) {
		_pts[i]._q = _pts[i]._pt - N03(_pts[i]._t) * _P0 - N33(_pts[i]._t) * _P3;
	}
  Geom::Point Q1(0, 0);
  Geom::Point Q2(0, 0);
	double m00 = 0, m01 = 0, m10 = 0, m11 = 0;
	for (int i=stP+1; i<enP; i++) {
		double n1= N13(_pts[i]._t);
		double n2= N23(_pts[i]._t);
		Q1 += n1 * _pts[i]._q;
		Q2 += n2 * _pts[i]._q;
		m00 += n1 * n1;
		m01 += n1 * n2;
		m10 += n2 * n1;
		m11 += n2 * n2;
	}
	double det = m00*m11 - m01*m10;
	if (fabs(det) > 0.00001) {
		double r00 = m11 / det;
		double r01 = -m01 / det;
		double r10 = -m10 / det;
		double r11 = m00 / det;

		_P1 = r00 * Q1 + r01 * Q2;
		_P2 = r10 * Q1 + r11 * Q2;
	}
}

void Polyline::nudgePositions(int stP, int enP) {
	_delta = 0;
	for (int i=stP+1; i<enP; i++) {
		double t = _pts[i]._t;
		// compute the current point on the curve
		Geom::Point p01 = (1-t) * _P0 + t * _P1;
		Geom::Point p12 = (1-t) * _P1 + t * _P2;
		Geom::Point p23 = (1-t) * _P2 + t * _P3;
		Geom::Point p012 = (1-t) * p01 + t * p12;
		Geom::Point p123 = (1-t) * p12 + t * p23;
		Geom::Point p0123 = (1-t) * p012 + t * p123;
		// compute the derivative of the current point on the curve
		Geom::Point d01 = (1-t) * (_P1 - _P0) + t * (_P2 - _P1);
		Geom::Point d12 = (1-t) * (_P2 - _P1) + t * (_P3 - _P2);
		Geom::Point d012 = (1-t) * d01 + t * d12;
		// compute the second derivative of the current point on the curve
		Geom::Point dd01 = (1-t) * (_P2 - (2.0 * _P1) + _P0) + t * (_P3 - (2.0 * _P2) + _P1);

		Geom::Point mp = p0123 - _pts[i]._pt;
		Geom::Point dm = d012;
		Geom::Point ddm = dd01;

		// distance
		_delta += dot(mp, mp);
		// derivative of the squared distance
		double f = 2 * dot(mp, dm);
		// second derivative of the squared distance
		double df = 2 * dot(mp, ddm) + 2 * dot(dm, dm);
		// newton-rhapson
		double dt = (fabs(df) < 0.00001) ? 0 : 0.5 * f / df;
		// clamp the new abcissis to the previous and next abcissis
		double nt = _pts[i]._t - dt;
		if (nt < _pts[i-1]._t)
			nt = _pts[i-1]._t;
		if (nt > _pts[i+1]._t)
			nt = _pts[i+1]._t;
		if (nt < 0)
			nt = 0;
		if (nt > 1)
			nt = 1;
		_pts[i]._t = nt;
	}
}

void Polyline::refreshDelta(int stP, int enP) {
	_delta = 0;
	for (int i=stP+1; i<enP; i++) {
		double t = _pts[i]._t;
		Geom::Point p01 = (1-t) * _P0 + t * _P1;
		Geom::Point p12 = (1-t) * _P1 + t * _P2;
		Geom::Point p23 = (1-t) * _P2 + t * _P3;
		Geom::Point p012 = (1-t) * p01 + t * p12;
		Geom::Point p123 = (1-t) * p12 + t * p23;
		Geom::Point p0123 = (1-t) * p012 + t * p123;

		Geom::Point mp = p0123 - _pts[i]._pt;

		_delta += dot(mp, mp);
	}
}

void Polyline::getSimplifiedPath(Geom::SVGPathWriter &pathWriter, double delta) {
  int nbElem = -1;
  bool approx = false;
  if (_pts.size() <= 0)
    return;
  pathWriter.moveTo(_pts[0]._pt);
  if (_pts.size() == 1)
    return;
  if (_pts.size() == 2) {
    pathWriter.lineTo(_pts[1]._pt);
    return;
  }

  int maxPatch = (_pts.size() + 2) / 3;
	if ( nbElem > 0 ) maxPatch = nbElem+1;
  std::vector<SimplifyPoint> best(_pts.size() * maxPatch);
  for(int i = 0; i < (_pts.size() * maxPatch); i++) { best.push_back(SimplifyPoint()); }
	int lastP = _pts.size() - 1;
	for (int enP = 1; enP <=lastP; enP++) {
		if (approx) {
			int stP = enP-1;
			int dStP = 1;
			double lastDelta = 0;
			int lastStP = enP;
			while (stP >= 0) {
				initPositions(stP, enP);
				calcControlPoints(stP, enP);

				nudgePositions(stP, enP);
				calcControlPoints(stP, enP);

				refreshDelta(stP, enP);

				for (int rStP = stP; rStP < lastStP; rStP++) {
					// interpolate the delta
					// when enP -20 <= stP < enP, then lastStP = stP + 1 so
					// exDelta end up being _delta
					double exDelta = (_delta * ((double)(lastStP - rStP))
							+ lastDelta * ((double)(rStP - stP)))
							/ ((double)(lastStP-stP));
					for (int n = 0; n<maxPatch-1; n++) {
						int stIdx = n * _pts.size() + stP;
						int enIdx = (n+1) * _pts.size() + enP;
						if (best[stIdx]._prev >= 0 || (stP == 0 && n == 0)) {
							double nDelta = best[stIdx]._delta + exDelta;
							if ( best[enIdx]._prev < 0 || best[enIdx]._delta> nDelta) {
								best[enIdx] = SimplifyPoint(stP, nDelta);
							}
						}
					}
				}
				lastDelta = _delta;
				lastStP = stP;
				if (enP - stP > 20)
					dStP = 10;
				if (enP - stP > 200)
					dStP = 100;
				stP -= dStP;
				if (stP < 0 && stP > -dStP)
					stP=0; // if stP was already 0, then it's < 0 after here, and
				           // the while() loop exits
			}
		} else {
			for (int stP=0; stP<enP; stP++) {
				initPositions(stP, enP);
				calcControlPoints(stP, enP);

				nudgePositions(stP, enP);
				calcControlPoints(stP, enP);

				refreshDelta(stP, enP);

				for (int n = 0; n<maxPatch-1; n++) {
					int stIdx = n * _pts.size() + stP;
					int enIdx = (n+1) * _pts.size() + enP;
					if (best[stIdx]._prev >= 0 || (stP == 0 && n == 0)) {
						double nDelta = best[stIdx]._delta + _delta;
						if ( best[enIdx]._prev < 0 || best[enIdx]._delta> nDelta) {
							best[enIdx] = SimplifyPoint(stP, nDelta);
						}
					}
				}
			}
		}
	}

//	for (int i=0; i<lastP; i++) {
//		printf("%i: ", i);
//		for (int n=0; n<maxPatch; n++) {
//			int idx = n * _pts.nb() + i;
//			printf("(%i %f) ", best[idx]._prev, best[idx]._delta);
//		}
//		printf("\n");
//	}

	double maxDelta = delta * ((double) lastP);
	int minNb = -1;
	double minDelta = -1;
	// find optimum
	if ( nbElem > 0 ) {
		for (int n=0; n<=nbElem; n++) {
			int idx = n * _pts.size() + lastP;
			if (best[idx]._prev >= 0) {
				if (minNb < 0 || best[idx]._delta < minDelta) {
					minDelta = best[idx]._delta;
					minNb = n;
				}
			}
		}
	} else if ( delta > 0 ) {
		for (int n=0; n<maxPatch; n++) {
			int idx = n * _pts.size() + lastP;
			if (best[idx]._prev >= 0) {
				if (best[idx]._delta < maxDelta) {
					if (minNb < 0 || n < minNb) {
						minNb = n;
					}
				}
			}
		}
	} else {
		// no target: neither a number of cubic patches, nor a max delta
		// from the curve -> do nothing...
	}

	if (minNb >= 0) {
		// backtrack in the matrix
    std::vector<int> poss;
		int curP = lastP;
		poss.push_back(curP);
		for (int curN=minNb; curN >= 0; curN--) {
			int idx = curN * _pts.size() + curP;
			int prev = best[idx]._prev;
			if (prev >= 0) {
				poss.push_back(prev);
			}
			curP = prev;
		}
//		printf("poss: ");
//		for (int i=0; i<poss.nb(); i++) {
//			printf("(%i %i) ", i, poss[i]);
//		}
//		printf("\n");
		// refit points to curves, because we didn't store the results
		// we fitted so many curves at this point, one or two more won't
		// make a big differnce
		// plus in the approximation mode, we didn't compute all curves
		for (int i=poss.size()-1; i>0; i--) {
			int enP = poss[i-1];
			int stP = poss[i];
			initPositions(stP, enP);
			calcControlPoints(stP, enP);

			nudgePositions(stP, enP);
			calcControlPoints(stP, enP);

			pathWriter.curveTo(_P1, _P2, _P3);
		}
	}
  return;
}

