#include <iostream>
#include <vector>
#include <algorithm>
#include <2geom/pathvector.h>
#include <2geom/svg-path-parser.h>
#include <2geom/svg-path-writer.h>
#include <2geom/interval.h>
#include <2geom/line.h>
#include <2geom/rect.h>
#include <2geom/path-sink.h>
#include <Polyline.h>

int main(void)
{
  std::string d = "M 13.619109,45.251086 35.58749,34.375312 57.667736,29.0406 80.646092,27.615036 h 23.281378 l 29.37957,1.665713 c 0,0 30.10018,19.354382 11.91913,49.402232 C 127.04513,108.73083 112.39063,105.82635 103.98001,104.2499 95.569399,102.67344 70.110884,103.5801 44.96753,88.190598 19.824176,72.801101 13.619109,45.251086 13.619109,45.251086 Z";

  Geom::PathVector pathv = Geom::parse_svg_path(d.c_str());
  for(auto pth: pathv) {
    Polyline polyline(pth, 0.01);
    Geom::SVGPathWriter pathWriter;
    polyline.getSimplifiedPath(pathWriter, 0.01);
    if (pth.closed())
      pathWriter.closePath();
    std::cout << pathWriter.str() << std::endl;
  }
  return 0;
}
