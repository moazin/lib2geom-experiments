#ifndef PATH_FLATTENER_H_
#define PATH_FLATTENER_H_
#include <2geom/pathvector.h>

class PathFlattener {
  enum point_descr {
    polyline_lineto = 0,
    polyline_moveto = 1
  };
  class FPoint {
    public:
      Geom::Point pt;
      point_descr flag;
      bool closed;
      FPoint(Geom::Point point, point_descr flag, bool closed = false): pt(point), flag(flag), closed(closed) {};
  };
  private:
    void breakLine(Geom::LineSegment const *line, double threshold, bool breakline);
  public:
    std::vector<FPoint> pts;
    PathFlattener(Geom::PathVector pathv, double threshold, bool convertLines = false);
    Geom::PathVector getSimplifiedPath(double threshold);
};
#endif
