#include <PathFlattener.h>
#include <PathSimplifier.h>

void PathFlattener::breakLine(Geom::LineSegment const *line, double threshold, bool break_line) {
  if(break_line) {
    Geom::Point stP = line->initialPoint();
    Geom::Point enP = line->finalPoint();
    double mag = Geom::L2(enP - stP);
    if((mag > threshold) && (threshold > 0)) {
      for(double i = threshold; i < mag; i += threshold)
      {
        Geom::Point nX = ((mag - i) * stP + i * enP) / mag;
        pts.push_back(PathFlattener::FPoint(nX, polyline_lineto));
      }
    }
  }
  pts.push_back(PathFlattener::FPoint(line->finalPoint(), polyline_lineto));
}

PathFlattener::PathFlattener(Geom::PathVector pathv, double threshold, bool convertLines) {
  for(auto &path: pathv) {
    pts.push_back(PathFlattener::FPoint(path.initialPoint(), polyline_moveto));
    for(auto &cv: path) {
      Geom::Curve const *curve = &cv;
      if(Geom::LineSegment const *line_segment = dynamic_cast<Geom::LineSegment const*>(curve)){
        breakLine(line_segment, threshold, convertLines);
      }
    }
  }
}

Geom::PathVector PathFlattener::getSimplifiedPath(double threshold) {
  int N = pts.size();
  bool path_started = false;
  PathSimplifier simplifier;
  for(int i = 0; i < N; i++)
  {
    simplifier.addPoint(pts[i].pt);
  }
  Geom::Path simplified_path = simplifier.getSimplifiedPath(threshold);
  Geom::PathVector pathv;
  pathv.push_back(simplified_path);
  return pathv;
}


