#include <PathSimplifier.h>
#include <PathFlattener.h>
#include <2geom/svg-path-parser.h>
#include <2geom/svg-path-writer.h>
#include <memory>
#include <chrono>

int main(void) {
  std::string svgd = "M 18.649542,265.85015 26.143083,205.07321 22.323122,154.3388 19.562396,123.16537 23.895442,93.184952 32.594073,69.526038 46.390438,56.124667 71.46809,39.67068 l 27.231004,-5.500997 28.134826,-7.128391 22.98658,4.361416 14.22942,7.526771 10.63501,20.89224 3.12958,24.859475 -2.72942,23.635386 -11.8754,28.85733 -23.34585,21.75912 -28.83138,12.21281 -43.513861,1.17654 -20.114077,-24.08091 -10.253993,-26.85496 7.089174,-24.082659 13.883966,-26.181227 20.911552,-9.43587";
  Geom::PathVector pathv = Geom::parse_svg_path(svgd.c_str());
  PathFlattener flattener(pathv, 0.1, true);
  Geom::PathVector spathv = flattener.getSimplifiedPath(0.05);
  std::string str_spathv = Geom::write_svg_path(spathv);
  std::cout  << str_spathv << std::endl;
  return 0;
}
