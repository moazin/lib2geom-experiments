#ifndef PATH_SIMPLIFIER_H_
#define PATH_SIMPLIFIER_H_

#include <memory>
#include <2geom/point.h>

#define N03(t) ((1.0-t)*(1.0-t)*(1.0-t))
#define N13(t) (3*(t)*(1.0-t)*(1.0-t))
#define N23(t) (3*(t)*(t)*(1.0-t))
#define N33(t) ((t)*(t)*(t))


class PathSimplifier {
  class FitPoint {
    public:
      Geom::Point _pt;
      Geom::Point _q;
      double      _l;
      double      _t;
      FitPoint(void) : _pt(), _q() { _l = 0; _t = 0; };
      FitPoint(Geom::Point point) : _pt(point), _q() { _l = 0; _t = 0; };
      virtual ~FitPoint(void) {};
  };
  private:
    std::vector<FitPoint> _pts;
    double      _delta;
    Geom::Point _P0;
    Geom::Point _P1;
    Geom::Point _P2;
    Geom::Point _P3;
    void initPositions(int stP, int enP);
    void nudgePositions(int stP, int enP);
    void calcControlPoints(int stP, int enP);
    void refreshDelta(int stP, int enP);
    bool fitCurve(int stP, int enP, double delta);
  public:
    PathSimplifier() : _pts() { }
    ~PathSimplifier() { }
    void flush();
    void addPoint(Geom::Point point);
    Geom::Path getSimplifiedPath(double delta);
};

#endif
