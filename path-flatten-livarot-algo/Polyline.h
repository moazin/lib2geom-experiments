#ifndef POLYLINE_H_
#define POLYLINE_H_

#include <2geom/pathvector.h>
#include <2geom/svg-path-writer.h>

#define N03(t) ((1.0-t)*(1.0-t)*(1.0-t))
#define N13(t) (3*(t)*(1.0-t)*(1.0-t))
#define N23(t) (3*(t)*(t)*(1.0-t))
#define N33(t) ((t)*(t)*(t))

class FitPoint {
  public:
    Geom::Point _pt;
    Geom::Point _q;
    double      _l;
    double      _t;
    FitPoint(void) : _pt(), _q() { _l = 0; _t = 0; };
    FitPoint(Geom::Point point) : _pt(point), _q() { _l = 0; _t = 0; };
    virtual ~FitPoint(void) {};
};

class Polyline {
  private:
    bool approximatesEnough(Geom::Curve &curve, Geom::Line& line, double threshold);
    void splitCurve(const Geom::Curve& curve, double t_start, double t_end, double threshold);

  public:
    std::vector<FitPoint> _pts;
    double      _delta;
    Geom::Point _P0;
    Geom::Point _P1;
    Geom::Point _P2;
    Geom::Point _P3;
    Polyline() : _pts() { }
    ~Polyline() { }
    Polyline(Geom::Path &path, double threshold);
    void addPoint(Geom::Point point);
    void initPositions(int stP, int enP);
    void nudgePositions(int stP, int enP);
    void calcControlPoints(int stP, int enP);
    void refreshDelta(int stP, int enP);
    bool fitCurve(int stP, int enP, double delta);
    void getSimplifiedPath(Geom::SVGPathWriter &pathWriter, double delta);
};

#endif
